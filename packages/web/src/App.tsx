import { ApolloProvider } from '@apollo/client';
import { CssBaseline, styled } from '@mui/material';
import createApolloClient from './graphql/client';
import HomePage from './pages/HomePage/HomePage';

const client = createApolloClient();
function App() {
  return (
    <ApolloProvider client={client}>
      <CssBaseline />
      <Background>
        <HomePage />
      </Background>
    </ApolloProvider>
  );
}

export default App;

const Background = styled('div')(({ theme }) => ({
  position: 'absolute',
  top: 0,
  right: 0,
  bottom: 0,
  left: 0,
  backgroundColor: theme.palette.grey[300],
}));
