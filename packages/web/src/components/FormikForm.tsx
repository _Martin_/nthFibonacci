import { styled } from '@mui/material';
import { Form } from 'formik';

interface Props {
  width?: string;
}

const FormikForm = styled(Form)<Props>(({ theme, width }) => ({
  width: width ? width : theme.spacing(40),
  margin: theme.spacing(0, 5.5),
  display: 'flex',
  flexDirection: 'column',
  '& >div:not(:last-child)': {
    marginBottom: theme.spacing(2),
  },
}));

export default FormikForm;
