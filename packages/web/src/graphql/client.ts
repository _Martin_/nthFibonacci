import { ApolloClient, from, HttpLink, InMemoryCache } from '@apollo/client';

const GRAPHQL_URI = `${process.env.REACT_APP_API_URL}/api/graphql?`;

const httpLink = new HttpLink({
  uri: GRAPHQL_URI,
});

export default function createClient() {
  return new ApolloClient({
    name: 'web',
    cache: new InMemoryCache({}),
    link: from([httpLink]),
    assumeImmutableResults: true,
  });
}
