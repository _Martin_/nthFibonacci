import { TypedDocumentNode as DocumentNode } from '@graphql-typed-document-node/core';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = {
  [K in keyof T]: T[K];
};
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & {
  [SubKey in K]?: Maybe<T[SubKey]>;
};
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & {
  [SubKey in K]: Maybe<T[SubKey]>;
};
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** A date-time string at UTC, such as 2019-12-03T09:54:33Z, compliant with the date-time format. */
  DateTime: string;
};

export type Dummy = {
  readonly __typename?: 'Dummy';
  readonly id: Scalars['ID'];
  /** Identifies the date and time when the object was created. */
  readonly createdAt: Scalars['DateTime'];
  /** Identifies the date and time when the object was last updated. */
  readonly updatedAt: Scalars['DateTime'];
  readonly isDummy: Scalars['Boolean'];
};

export type NthFibonacci = {
  readonly __typename?: 'NthFibonacci';
  readonly n: Scalars['Float'];
  readonly nthFibonacci: Scalars['Float'];
};

export type NthFibonacciInput = {
  readonly n: Scalars['Int'];
};

export type Query = {
  readonly __typename?: 'Query';
  readonly dummy: Dummy;
  readonly nthFibonacci: NthFibonacci;
};

export type QueryNthFibonacciArgs = {
  nthFibonacciInput: NthFibonacciInput;
};

export type NthFibonacciQueryVariables = Exact<{
  nthFibonacciInput: NthFibonacciInput;
}>;

export type NthFibonacciQuery = {
  readonly __typename?: 'Query';
  readonly nthFibonacci: {
    readonly __typename?: 'NthFibonacci';
    readonly n: number;
    readonly nthFibonacci: number;
  };
};

export const NthFibonacciDocument = {
  kind: 'Document',
  definitions: [
    {
      kind: 'OperationDefinition',
      operation: 'query',
      name: { kind: 'Name', value: 'NthFibonacci' },
      variableDefinitions: [
        {
          kind: 'VariableDefinition',
          variable: {
            kind: 'Variable',
            name: { kind: 'Name', value: 'nthFibonacciInput' },
          },
          type: {
            kind: 'NonNullType',
            type: {
              kind: 'NamedType',
              name: { kind: 'Name', value: 'NthFibonacciInput' },
            },
          },
        },
      ],
      selectionSet: {
        kind: 'SelectionSet',
        selections: [
          {
            kind: 'Field',
            name: { kind: 'Name', value: 'nthFibonacci' },
            arguments: [
              {
                kind: 'Argument',
                name: { kind: 'Name', value: 'nthFibonacciInput' },
                value: {
                  kind: 'Variable',
                  name: { kind: 'Name', value: 'nthFibonacciInput' },
                },
              },
            ],
            selectionSet: {
              kind: 'SelectionSet',
              selections: [
                { kind: 'Field', name: { kind: 'Name', value: 'n' } },
                {
                  kind: 'Field',
                  name: { kind: 'Name', value: 'nthFibonacci' },
                },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<NthFibonacciQuery, NthFibonacciQueryVariables>;
