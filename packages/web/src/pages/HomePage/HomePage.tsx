import { useLazyQuery } from '@apollo/client';
import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  styled,
  Typography,
} from '@mui/material';
import { Field, Formik } from 'formik';
import { TextField } from 'formik-mui';
import { useState } from 'react';
import * as Yup from 'yup';
import FormikForm from '../../components/FormikForm';
import { NthFibonacciDocument } from '../../graphql/graphql-operations';

interface NthFibForm {
  n: number;
}

interface Result {
  dialogOpen: boolean;
  n?: number;
  nthFibonacci?: number;
}

const HomePage = () => {
  const [result, setResult] = useState<Result>({ dialogOpen: false });
  const [getNthFibonacci] = useLazyQuery(NthFibonacciDocument, {
    notifyOnNetworkStatusChange: true,
    fetchPolicy: 'cache-first',
    onError: (err) => console.log({ err }), // we could also provide some UI feedback in case of error
    onCompleted: (data) =>
      setResult({ dialogOpen: true, ...data.nthFibonacci }),
  });
  const handleFormSubmit = async ({ n }: NthFibForm) => {
    if (result.n === n) {
      setResult((prev) => ({ ...prev, dialogOpen: true }));
    } else {
      getNthFibonacci({ variables: { nthFibonacciInput: { n } } });
    }
  };

  const handleDialogClose = () =>
    setResult((prev) => ({ ...prev, dialogOpen: false }));

  return (
    <ContentWrapper>
      <Formik<NthFibForm>
        initialValues={{ n: 2 }}
        onSubmit={handleFormSubmit}
        validationSchema={schema}
      >
        {({ isSubmitting, isValid }) => (
          <FormikForm>
            <Field
              name="n"
              component={TextField}
              variant="outlined"
              type="number"
              required
              label={'Fill in desired n'}
            />
            <Button
              type="submit"
              variant="contained"
              color="primary"
              disabled={isSubmitting || !isValid}
            >
              Calculate
            </Button>
          </FormikForm>
        )}
      </Formik>
      <Dialog open={result.dialogOpen} onClose={handleDialogClose}>
        <Box padding={2}>
          <DialogContent>
            <Typography>
              {`For n=${result.n} Fibonacci number is ${result.nthFibonacci}`}
            </Typography>
          </DialogContent>
          <DialogActions>
            <Button
              onClick={handleDialogClose}
              variant="contained"
              color="primary"
            >
              Close
            </Button>
          </DialogActions>
        </Box>
      </Dialog>
    </ContentWrapper>
  );
};

export default HomePage;

const ContentWrapper = styled('div')({
  marginTop: '25vh',
  display: 'flex',
  justifyContent: 'center',
});

const schema = Yup.object().shape({
  n: Yup.number().required().min(0).max(1450), // 1450 is current server limitation
});
