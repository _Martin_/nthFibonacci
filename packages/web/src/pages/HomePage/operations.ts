import { gql } from '@apollo/client';

export const nthFibonacci = gql`
  query NthFibonacci($nthFibonacciInput: NthFibonacciInput!) {
    nthFibonacci(nthFibonacciInput: $nthFibonacciInput) {
      n
      nthFibonacci
    }
  }
`;
