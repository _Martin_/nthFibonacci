import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { DummyModule } from './dummny/dummy.module';
import { NthFibonacci } from './nthFibonacci/nthFibonacci.module';

@Module({
  imports: [
    GraphQLModule.forRoot({
      path: '/api',
      autoSchemaFile: 'schema.gql',
      subscriptions: {
        'graphql-ws': {
          path: '/api/graphql',
        },
      },
    }),
    DummyModule,
    NthFibonacci,
  ],
})
export class AppModule {}
