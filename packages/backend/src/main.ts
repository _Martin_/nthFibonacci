import { Logger, ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const logger = new Logger('Application');
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(new ValidationPipe());
  app.setGlobalPrefix('/api');
  await app.listen(4000);
  console.log(`Application is running on: ${await app.getUrl()}`);

  app.enableShutdownHooks();
  const handleShutDown = (signal: string) =>
    logger.log(`Received ${signal} server is shutting down`);
  process.on('SIGINT', handleShutDown);
  process.on('SIGTERM', handleShutDown);
}
bootstrap();
