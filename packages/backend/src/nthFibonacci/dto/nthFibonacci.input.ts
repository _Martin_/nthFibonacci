import { Field, InputType, Int } from '@nestjs/graphql';
import { Max, Min } from 'class-validator';

@InputType()
export class NthFibonacciInput {
  @Min(0)
  @Max(1450)
  @Field(() => Int)
  n!: number;
}
