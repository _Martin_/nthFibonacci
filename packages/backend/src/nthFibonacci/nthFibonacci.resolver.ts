import { Args, Query, Resolver } from '@nestjs/graphql';
import { NthFibonacciInput } from './dto/nthFibonacci.input';
import { NthFibonacci } from './models/nthFibonacci.model';
import { NthFibonacciService } from './services/nthFibonacci.service';

@Resolver(() => NthFibonacci)
export class NthFibonacciResolver {
  constructor(private readonly nthFibonacciService: NthFibonacciService) {}

  @Query(() => NthFibonacci)
  async nthFibonacci(@Args('nthFibonacciInput') input: NthFibonacciInput) {
    const nthFibonacci = this.nthFibonacciService.nthFibonacci(input);
    return {
      n: input.n,
      nthFibonacci,
    };
  }
}
