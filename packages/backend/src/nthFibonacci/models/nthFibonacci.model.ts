import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class NthFibonacci {
  @Field(() => Number)
  n!: number;
  @Field(() => Number)
  nthFibonacci!: number;
}
