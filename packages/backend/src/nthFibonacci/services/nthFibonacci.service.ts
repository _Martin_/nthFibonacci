import { Injectable } from '@nestjs/common';
import { NthFibonacciInput } from '../dto/nthFibonacci.input';

@Injectable()
export class NthFibonacciService {
  // we cloud use Redis to store this cache, than when server restarts
  // we won't loose it. Also using using Redis the cache can be shared
  // across multiple instances
  private fibonacciCache: Map<number, number>;
  constructor() {
    this.fibonacciCache = new Map<number, number>([
      [0, 0],
      [1, 1],
    ]);
  }

  nthFibonacci(input: NthFibonacciInput): number {
    const { n } = input;
    let nthFib = this.fibonacciCache.get(n);
    if (nthFib != null) return nthFib;
    nthFib = this.nthFibonacci({ n: n - 1 }) + this.nthFibonacci({ n: n - 2 });
    this.fibonacciCache.set(n, nthFib);
    return nthFib;
  }
}
