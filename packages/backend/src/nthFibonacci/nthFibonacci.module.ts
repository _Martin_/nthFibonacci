import { Module } from '@nestjs/common';
import { NthFibonacciResolver } from './nthFibonacci.resolver';
import { NthFibonacciService } from './services/nthFibonacci.service';

@Module({
  providers: [NthFibonacciResolver, NthFibonacciService],
})
export class NthFibonacci {}
