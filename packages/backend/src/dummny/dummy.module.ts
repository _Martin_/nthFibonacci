import { Module } from '@nestjs/common';
import { DummyResolver } from './dummy.resolver';

@Module({
  providers: [DummyResolver],
})
export class DummyModule {}
