import { ObjectType } from '@nestjs/graphql';
import { BaseModel } from 'src/common/base.model';

@ObjectType()
export class Dummy extends BaseModel {
  isDummy!: boolean;
}
