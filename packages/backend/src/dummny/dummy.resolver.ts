import { Query, Resolver } from '@nestjs/graphql';
import { Dummy } from './dummy.model';

@Resolver(() => Dummy)
export class DummyResolver {
  @Query(() => Dummy)
  async dummy() {
    return {
      id: 1,
      createdAt: new Date(),
      updatedAt: new Date(),
      isDummy: true,
    };
  }
}
