# Backend pro Help4Ukraine

## Prvotní spuštění

1.  Nainstalování všech potřebných knihoven proveďte spuštěním příkazu `yarn`
2.  Zkopírujte soubor `.env.example`, přejmenujte na `.env` a vyplňte všechny proměnné
3.  Server poté zapnete spuštěním příkazu `yarn start` či `yarn start:dev`.

## Endpoints

1.  Server se spustí na portu 4000, zatím není konfigurovatelné (lze změnit v souboru main.ts). Pokud se server v pořádku zapne, tak adresu na které běží vypíše do konzole.
2.  GraphQl endpoint běží na http://[::1]:4000/api/graphql?
